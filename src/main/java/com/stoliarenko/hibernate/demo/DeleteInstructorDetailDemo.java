package com.stoliarenko.hibernate.demo;

import com.stoliarenko.hibernate.demo.entity.Instructor;
import com.stoliarenko.hibernate.demo.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteInstructorDetailDemo {

    public static void main(String[] args) {
        //create session factory
        SessionFactory factory = getSessionFactory();
        //create session
        Session session = factory.getCurrentSession();
        try{


            //start a transaction
            session.beginTransaction();

            //get the instructor detail object
            int theId = 4;
            InstructorDetail tempInstructorDetail = session.get(InstructorDetail.class, theId);

            //print the instructor detail
            System.out.println("temInstructorDetail " + tempInstructorDetail);

            //print the associated instructor
            System.out.println("the associated instructor: " + tempInstructorDetail.getInstructor());

            //now let's delete the instructor detail
            System.out.println("Deleting tempInstructorDetail "+ tempInstructorDetail );


            //remove the associated object reference
            //break bi-direction link

            tempInstructorDetail.getInstructor().setInstructorDetail(null);

            session.delete(tempInstructorDetail);

            //commit transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }
    }

    public static SessionFactory getSessionFactory() {
        return new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addAnnotatedClass(Instructor.class)
                    .addAnnotatedClass(InstructorDetail.class)
                    .buildSessionFactory();
    }
}
